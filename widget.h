#ifndef WIDGET_H
#define WIDGET_H
#include <QMainWindow>
#include <QFileDialog>
#include <QTableWidget>
#include <QDebug>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <QListWidget>
#include <QMessageBox>

QT_BEGIN_NAMESPACE
namespace Ui { class Widget; }
QT_END_NAMESPACE

class Widget : public QMainWindow
{
	Q_OBJECT
	
	struct columnName
	{
		QString comV;
		QString ipV;
		QString nameV;
		QString osV;
		QString ncV;
		QString statusV;
		QString uuidV;
	};
	struct Mydlpclipboard {
		QString ip;
		QString uuid;
		QString version;
	};
	struct Mydlpfs {
		QString ip;
		QString uuid;
		QString version;
	};
	struct Mydlpmail {
		QString ip;
		QString uuid;
		QString version;
	};
	struct Mydlpprinter {
		QString ip;
		QString uuid;
		QString version;
	};
	struct Mydlpweb {
		QString ip;
		QString uuid;
		QString version;
	};
	struct MyTableStruct{
		columnName computer;
		columnName ip;
		Mydlpclipboard mydlpclipboard;
		Mydlpfs mydlpfs;
		Mydlpmail mydlpmail;
		Mydlpprinter mydlpprinter;
		Mydlpweb mydlpweb;
		columnName name;
		columnName nc_version;
		columnName os_version;
		columnName status;
		columnName uuid;
	} MyTable;
	QVector<MyTableStruct> endpoints;
	QJsonValue value;
	
public:
	Widget(QWidget *parent = nullptr);
	~Widget();
	
protected:
	void on_tableWidget_itemDoubleClicked(QTableWidgetItem *item);
	
private slots:
	void on_searchButton_clicked();
	void TableWidgetDisplay(QVector<MyTableStruct>MyTable);
	void on_showFolderContentsButton_clicked();
	void on_chooseDirectoryButton_clicked();
	void on_listWidget_itemDoubleClicked(QListWidgetItem *item);
	void on_clearButton_clicked();
	void on_exitButton_clicked();
	void on_FilterExitButton_clicked();
	QVector<MyTableStruct> defineRandomData(int indexSize);
	
private:
	Ui::Widget *ui;
	bool clicked = false;
};


#endif // WIDGET_H
