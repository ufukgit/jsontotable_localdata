It is a program that transfers local json data to json table. There were files with the log extension in a local folder. These files contained raw data. The folder containing the log extension files was projected onto the screen with the list widget. This raw data is base64 decoded. The decoded data is now in json format. The data was turned into a key-value by reading and making the necessary json transformations (parse operation). Since the
key of the json data is "endpoints", its value is obtained by entering "endpoints". The parsed meaningful data was transferred to the created table.
      When the program is run;
        An empty interface with listwidget and tablewidget is opened to the user.
        The user selects the folder where the log files are located.
        The user selects any file inside the folder.
        (The data of the selected file in the background is decoded and the necessary transformations are made, and parsing is completed.)
        The data of the folder is reflected in the table.
