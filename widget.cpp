#include "widget.h"
#include "ui_widget.h"

static QString base64_decode(QString DecodeValue);
Widget::Widget(QWidget *parent)
	: QMainWindow(parent)
	, ui(new Ui::Widget){
	ui->setupUi(this);
	QVector<MyTableStruct> tableList;
	int indexSize = 500;
	tableList = defineRandomData(indexSize);
	TableWidgetDisplay(tableList);
}

Widget::~Widget()
{
	delete ui;
}

QVector<Widget::MyTableStruct>Widget::defineRandomData(int indexSize)
{
	QVector<MyTableStruct>tableList;
	for (int i = 0; i < indexSize; i++) {
		MyTableStruct MyTable;
		MyTable.computer.comV = "";
		MyTable.ip.ipV = "";
		MyTable.mydlpclipboard.ip = "";
		MyTable.mydlpclipboard.uuid = "";
		MyTable.mydlpclipboard.version = "";
		MyTable.mydlpfs.ip = "";
		MyTable.mydlpfs.uuid = "";
		MyTable.mydlpfs.version = "";
		MyTable.mydlpmail.ip = "";
		MyTable.mydlpmail.uuid = "";
		MyTable.mydlpmail.version = "";
		MyTable.mydlpprinter.ip = "";
		MyTable.mydlpprinter.uuid = "";
		MyTable.mydlpprinter.version = "";
		MyTable.mydlpweb.ip = "";
		MyTable.mydlpweb.uuid = "";
		MyTable.mydlpweb.version = "";
		MyTable.name.nameV = "";
		MyTable.nc_version.ncV = "";
		MyTable.os_version.osV = "";
		MyTable.status.statusV = "";
		MyTable.uuid.uuidV = "";
		tableList.push_back(MyTable);
	}
	return tableList;
}

void Widget::on_chooseDirectoryButton_clicked()
{
	clicked = true;
	QString filename= QFileDialog::getExistingDirectory(this,"Choose Directory");
	if (filename.isEmpty())
		return;
	ui->lineEdit->setText(filename);
}

void Widget::on_showFolderContentsButton_clicked()
{
	if (clicked == false) {
		QMessageBox::information(this, tr("WARNING"), tr("Press Choose Directory Button"));
		return;
	}
	ui->listWidget->clear();
	QString dirPath= ui->lineEdit->text();
	if (dirPath.isEmpty())
		return;
	QDir dir(dirPath);
	if (dir.isEmpty())
		return;
	QList <QFileInfo> fileList= dir.entryInfoList();
	if (!fileList.size())
		return;
	for (int i=2; i < fileList.size(); i++) {
		ui->listWidget->addItem(fileList.at(i).absoluteFilePath());
	}
}

void Widget::on_listWidget_itemDoubleClicked(QListWidgetItem *item)
{
	endpoints.clear();
	QFile tmpfile(item->text());
	if(!tmpfile.open(QIODevice::ReadOnly | QIODevice::Text))
		return;
	QByteArray rawData = tmpfile.readAll();
	tmpfile.close();
	if(rawData.isEmpty())
		return;
	QJsonDocument doc(QJsonDocument::fromJson(rawData));
	if (doc.isEmpty())
		return;
	QJsonObject json = doc.object();
	if (json.isEmpty())
		return;
	foreach(const QString& key, json.keys()){
		QJsonValue value = json.value(key);
		if (value.isNull())
			return;
		QString valueString = value.toString();
		if (valueString.isEmpty())
			return;
		QString decodedString = base64_decode(valueString);
		if (decodedString.isEmpty())
			return;
		QString data = decodedString;
		QJsonDocument doc = QJsonDocument::fromJson(data.toUtf8());
		if (doc.isEmpty())
			return;
		QJsonObject rootObj = doc.object();
		if (rootObj.isEmpty())
			return;
		QJsonValue endpointsValue = rootObj.value("endpoints");
		if (endpointsValue.isNull())
			return;
		if(endpointsValue.type() == QJsonValue::Array){
			QJsonArray endpointsArray = endpointsValue.toArray();
			if (endpointsArray.isEmpty())
				return;
			for (QJsonValue arr: endpointsArray){
				MyTableStruct table;
				QJsonObject obj = arr.toObject();
				if (obj.contains("computer")){
					columnName computerValue;
					computerValue.comV = obj.value("computer").toString();
					table.computer = computerValue;
				}
				if (obj.contains("ip")){
					columnName ipValue;
					ipValue.ipV = obj.value("ip").toString();
					table.ip = ipValue;
				}
				if (obj.contains("name")){
					columnName nameValue;
					nameValue.nameV = obj.value("name").toString();
					table.name = nameValue;
				}
				if (obj.contains("nc_version")){
					columnName ncValue;
					ncValue.ncV = obj.value("nc_version").toString();
					table.nc_version = ncValue;
				}
				if (obj.contains("os_version")){
					columnName osValue;
					osValue.osV = obj.value("os_version").toString();
					table.os_version = osValue;
				}
				if (obj.contains("status")){
					bool statusBool = obj.value("status").toBool();
					table.status.statusV = statusBool ? "online" : "offline";
				}
				if (obj.contains("uuid")){
					columnName uuidValue;
					uuidValue.uuidV = obj.value("uuid").toString();
					table.uuid = uuidValue;
				}
				if (obj.contains("mydlpclipboard")){
					QJsonObject dlpclipboardObj = obj.value("mydlpclipboard").toObject();
					Mydlpclipboard mydlpValue;
					mydlpValue.ip = dlpclipboardObj.value("ip").toString();
					mydlpValue.uuid = dlpclipboardObj.value("uuid").toString();
					mydlpValue.version = dlpclipboardObj.value("version").toString();
					table.mydlpclipboard = mydlpValue;
				}
				if (obj.contains("mydlpfs")){
					QJsonObject dlpfsObj = obj.value("mydlpfs").toObject();
					Mydlpfs mydlpValue;
					mydlpValue.ip = dlpfsObj.value("ip").toString();
					mydlpValue.uuid = dlpfsObj.value("uuid").toString();
					mydlpValue.version = dlpfsObj.value("version").toString();
					table.mydlpfs = mydlpValue;
				}
				if (obj.contains("mydlpmail")){
					QJsonObject dlpmailObj = obj.value("mydlpmail").toObject();
					Mydlpmail mydlpValue;
					mydlpValue.ip = dlpmailObj.value("ip").toString();
					mydlpValue.uuid = dlpmailObj.value("uuid").toString();
					mydlpValue.version = dlpmailObj.value("version").toString();
					table.mydlpmail = mydlpValue;
				}
				if (obj.contains("mydlpprinter")){
					QJsonObject dlpprinterObj = obj.value("mydlpprinter").toObject();
					Mydlpprinter mydlpValue;
					mydlpValue.ip = dlpprinterObj.value("ip").toString();
					mydlpValue.uuid = dlpprinterObj.value("uuid").toString();
					mydlpValue.version = dlpprinterObj.value("version").toString();
					table.mydlpprinter = mydlpValue;
				}
				if (obj.contains("mydlpweb")){
					QJsonObject dlpwebObj = obj.value("mydlpweb").toObject();
					Mydlpweb mydlpValue;
					mydlpValue.ip = dlpwebObj.value("ip").toString();
					mydlpValue.uuid = dlpwebObj.value("uuid").toString();
					mydlpValue.version = dlpwebObj.value("version").toString();
					table.mydlpweb = mydlpValue;
				}
				endpoints.push_back(table);
			}
		}
		TableWidgetDisplay(endpoints);
	}
}

QString base64_decode(QString DecodeValue)
{
	QByteArray ba;
	ba.append(DecodeValue);
	return QByteArray::fromBase64(ba);
}

void Widget::TableWidgetDisplay(QVector<MyTableStruct>tableList)
{
	QTableWidget *table = ui->tableWidget;
	table->setColumnCount(12);
	table->setRowCount(tableList.size());
	QStringList upLabels;
	upLabels
			<<"computer"
		   <<"ip"
		  <<"name"
		 <<"nc_version"
		<<"os_version"
	   <<"status"
	  <<"uuid"
	 <<"mydlpclipboard"
	<<"mydlpfs"
	<<"mydlpemail"
	<<"mydlpprinter"
	<<"mydlpweb";
	if (!upLabels.size())
		return;
	table->setHorizontalHeaderLabels(upLabels);
	table->setStyleSheet(
				"QTableWidget{"
				"background-color: #C0C0C0;"
				"alternate-background-color: #606060;"
				"selection-background-color: #282828;"
				"}");
	table->setAlternatingRowColors(true);
	table->setSelectionMode(QAbstractItemView::SingleSelection);
	table->setSelectionBehavior(QAbstractItemView::SelectRows);
	for(int i=0; i<tableList.size(); i++) {
		auto ep = tableList[i];
		{
			QTableWidgetItem *item = new QTableWidgetItem;
			item->setText(QString("%1").arg(ep.computer.comV));
			table->setItem(i, 0, item);
		}
		{
			QTableWidgetItem *item = new QTableWidgetItem;
			item->setText(QString("%1").arg(ep.ip.ipV));
			table->setItem(i, 1, item);
		}
		{
			QTableWidgetItem *item = new QTableWidgetItem;
			item->setText(QString("%1").arg(ep.name.nameV));
			table->setItem(i, 2, item);
		}
		{
			QTableWidgetItem *item = new QTableWidgetItem;
			item->setText(QString("%1").arg(ep.nc_version.ncV));
			table->setItem(i, 3, item);
		}
		{
			QTableWidgetItem *item = new QTableWidgetItem;
			item->setText(QString("%1").arg(ep.os_version.osV));
			table->setItem(i, 4, item);
		}
		{
			QTableWidgetItem *item = new QTableWidgetItem;
			item->setText(QString("%1").arg(ep.status.statusV));
			table->setItem(i, 5, item);
		}
		{
			QTableWidgetItem *item = new QTableWidgetItem;
			item->setText(QString("%1").arg(ep.uuid.uuidV));
			table->setItem(i, 6, item);
		}
		{
			QTableWidgetItem *item = new QTableWidgetItem;
			item->setText(QString("%1 \n %2 \n %3").arg(ep.mydlpclipboard.ip).arg(ep.mydlpclipboard.uuid).arg(ep.mydlpclipboard.version));
			table->setItem(i, 7, item);
		}
		{
			QTableWidgetItem *item = new QTableWidgetItem;
			item->setText(QString("%1 \n %2 \n %3").arg(ep.mydlpfs.ip).arg(ep.mydlpfs.uuid).arg(ep.mydlpfs.version));
			table->setItem(i, 8, item);
		}
		{
			QTableWidgetItem *item = new QTableWidgetItem;
			item->setText(QString("%1 \n %2 \n %3").arg(ep.mydlpmail.ip).arg(ep.mydlpmail.uuid).arg(ep.mydlpmail.version));
			table->setItem(i, 9, item);
		}
		{
			QTableWidgetItem *item = new QTableWidgetItem;
			item->setText(QString("%1  \n %2 \n %3").arg(ep.mydlpprinter.ip).arg(ep.mydlpprinter.uuid).arg(ep.mydlpprinter.version));
			table->setItem(i, 10, item);
		}
		{
			QTableWidgetItem *item = new QTableWidgetItem;
			item->setText(QString("%1 \n %2 \n %3").arg(ep.mydlpweb.ip).arg(ep.mydlpweb.uuid).arg(ep.mydlpweb.version));
			table->setItem(i, 11, item);
		}
	}
}

void Widget::on_clearButton_clicked()
{
	ui->tableWidget->setRowCount(0);
}

void Widget::on_exitButton_clicked()
{
	this->close();
}

void Widget::on_searchButton_clicked()
{
	QTableWidget *table = ui->tableWidget;
	QString filter = ui->filterline->text();
	for (int i = 0; i< table->rowCount(); i++) {
		bool match = true;
		for(int j=0; j<table->columnCount(); j++) {
			QTableWidgetItem  *item = table->item(i,j);
			if(item->text().contains(filter)) {
				match = false;
				break;
			}
		}
		table->setRowHidden(i,match);
	}
}

void Widget::on_FilterExitButton_clicked()
{
	ui->filterline->clear();
	ui->tableWidget->setRowCount(0);
	TableWidgetDisplay(endpoints);
}
